package com.Robusta.testcases.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddToCartPage extends PageBase {
    public AddToCartPage(WebDriver driver) {
        super(driver);
    }


    private By addToCartButton = By.xpath("//*[@data-cy='add-to-card']");
    private By continueShopping = By.linkText("Continue Shopping");


    public void addItemToCart() {
        clickButton(addToCartButton);
    }

    public void continueShopping() {
        clickButton(continueShopping);
    }

}
