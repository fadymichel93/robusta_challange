package com.Robusta.testcases.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends PageBase {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    private By searchInput = By.id("searchInput");
    private By searchValue = By.xpath("//div[@data-cy-region='search-bar']//li[1]");
    private By openCart = By.xpath("//button[@aria-label='Show Cart']//*[local-name()='svg']");


    public void setData(String data) {
        setText(data, searchInput);
    }

    public void clickOnValue() {
        clickButton(searchValue);
    }

    public void openCart() {
        clickButton(openCart);
    }
}
