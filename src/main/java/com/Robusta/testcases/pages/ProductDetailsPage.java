package com.Robusta.testcases.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductDetailsPage extends PageBase {
    public ProductDetailsPage(WebDriver driver) {
        super(driver);

    }

    private By originalPrice = By.xpath("//*[@id=\"main\"]/div/div/div/div/div[2]/div[4]/div[1]/a/div/div[2]/span[1]");
    public By iceBoxProduct8L = By.xpath("//div[@class='h-full w-full']//div[1]//a[1]");
    public By iceBoxProduct16L = By.xpath("//div[@class='h-full w-full']//div[2]//a[1]");
    public By iceBoxProduct48L = By.xpath("//div[@class='h-full w-full']//div[3]//a[1]");

    public boolean priceIsDisplayed() {
        return elementIsDisplayed(originalPrice);
    }

    public void addFirstItem() {
        clickButton(iceBoxProduct8L);
    }

    public void addSecondItem() {
        clickButton(iceBoxProduct16L);
    }

    public void addThirdItem() {
        clickButton(iceBoxProduct48L);
    }



}

