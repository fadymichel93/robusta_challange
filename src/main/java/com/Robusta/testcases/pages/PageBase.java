package com.Robusta.testcases.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {
    WebDriver driver;
    WebDriverWait wait;

    public PageBase(WebDriver driver) {
        this.driver = driver;
    }

    protected WebElement find(By locator) {
        return driver.findElement(locator);
    }

    protected void setText(String data, By locator) {
        waitElement(locator);
        find(locator).clear();
        find(locator).sendKeys(data);
    }

    protected void clickButton(By locator) {
        waitElement(locator);
        find(locator).click();
    }

    protected void waitElement(By locator) {
        wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected boolean elementIsDisplayed(By locator) {
        waitElement(locator);
        return find(locator).isDisplayed();

    }


}
