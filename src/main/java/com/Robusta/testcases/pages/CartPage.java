package com.Robusta.testcases.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage extends PageBase {
    public CartPage(WebDriver driver) {
        super(driver);
    }

    private By removeProduct = By.xpath("//*[@id=\"app\"]/header/div[2]/div/div[4]/div[2]/div[2]/div/div[3]/button");

    public void removeProductsFromCart() {
        for (int i = 1; i < 4; i++) {
            clickButton(removeProduct);
        }

    }

}
