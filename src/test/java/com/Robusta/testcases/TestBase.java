package com.Robusta.testcases;

import com.Robusta.testcases.pages.AddToCartPage;
import com.Robusta.testcases.pages.CartPage;
import com.Robusta.testcases.pages.HomePage;
import com.Robusta.testcases.pages.ProductDetailsPage;
import com.Robusta.utils.ExcelFileManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.IOException;

public class TestBase {
    protected static WebDriver driver;
    protected ExcelFileManager fileManager;
    protected String URL;

    @BeforeClass
    public void setUp() throws IOException {
        WebDriverManager.chromedriver().setup();
        fileManager = new ExcelFileManager();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to(URL = fileManager.setExcelFile(2, 1));
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }


}
