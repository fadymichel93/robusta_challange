package com.Robusta.testcases;

import com.Robusta.testcases.pages.HomePage;
import com.Robusta.testcases.pages.ProductDetailsPage;
import com.Robusta.utils.ExcelFileManager;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;


public class ValidateToSearch extends TestBase {
    HomePage homePage;
    ExcelFileManager fileManager;
    ProductDetailsPage detailsPage;

    @Test()
    public void setDataToSearchInput() throws IOException {
        homePage = new HomePage(driver);
        detailsPage = new ProductDetailsPage(driver);
        fileManager = new ExcelFileManager();
        homePage.setData(fileManager.setExcelFile(1, 1));
        homePage.clickOnValue();
        Assert.assertTrue(detailsPage.priceIsDisplayed());
    }

}
