package com.Robusta.testcases;

import com.Robusta.testcases.pages.AddToCartPage;
import com.Robusta.testcases.pages.CartPage;
import com.Robusta.testcases.pages.HomePage;
import com.Robusta.testcases.pages.ProductDetailsPage;
import com.Robusta.utils.ExcelFileManager;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class AddAndRemoveFromCartTest extends TestBase {
    ProductDetailsPage detailsPage;
    AddToCartPage toCartPage;
    HomePage homePage;
    ExcelFileManager fileManager;
    CartPage page;

    @Test
    public void addItemsToCart() throws IOException, InterruptedException {
        homePage = new HomePage(driver);
        detailsPage = new ProductDetailsPage(driver);
        toCartPage = new AddToCartPage(driver);
        page = new CartPage(driver);
        fileManager = new ExcelFileManager();
        homePage.setData(fileManager.setExcelFile(1, 1));
        homePage.clickOnValue();
        detailsPage.addFirstItem();
        toCartPage.addItemToCart();
        toCartPage.continueShopping();

        homePage.setData(fileManager.setExcelFile(1, 1));
        homePage.clickOnValue();
        detailsPage.addSecondItem();
        toCartPage.addItemToCart();
        toCartPage.continueShopping();

        homePage.setData(fileManager.setExcelFile(1, 1));
        homePage.clickOnValue();
        detailsPage.addThirdItem();
        toCartPage.addItemToCart();
        toCartPage.continueShopping();
        Thread.sleep(3000);
    }
        @Test
        public void removeProducts (){
            homePage = new HomePage(driver);
            homePage.openCart();
            page.removeProductsFromCart();
        }

    }
